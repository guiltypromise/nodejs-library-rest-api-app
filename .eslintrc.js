module.exports = {
    extends: 'airbnb-base',
    rules: {
        'comma-dangle': 0,
        'linebreak-style': 0,
        indent: 0,
        'no-multiple-empty-lines': 0,
        'import/no-extraneous-dependencies': 0,
        'no-trailing-spaces': 0,
        'padded-blocks': 0,
        'object-curly-spacing': 0,
        'eol-last': 0,
        'space-before-blocks': 0,
        'keyword-spacing': 0,
        'no-unused-vars': 0,
        'spaced-comment': 0,
    },
    env: {
        node: true,
        mocha: true
    }
};
