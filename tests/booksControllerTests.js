const should = require('should');
const sinon = require('sinon');

const booksController = require('../controllers/booksController');

// BDD style testing

describe('Book Controller Tests:', () => {
    describe('POST', () => {
        it('should not allow an empty title on POST', () => {
            // mock a Book
            const Book = function (book) {
                this.save = () => {};
            };
            // mock the request
            const request = {
                body: {
                    author: 'AuthorTest'
                }
            };

            const response = {
                status: sinon.spy(), // tracking analytics on these attributes
                send: sinon.spy(),
                json: sinon.spy(),
            };

            const controller = booksController(Book);

            controller.post(request, response);

            // test response is 400 BAD_REQUEST
            // and show status response
            response.status.calledWith(400).should.equal(true, `Bad Status ${response.status.args[0][0]}`);

            // also need a validation message returned
            response.send.calledWith('Title is required').should.equal(true);
        });
    });
});

