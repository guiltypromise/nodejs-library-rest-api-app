require('should');
const request = require('supertest'); // supertest as 'request' 
const mongoose = require('mongoose');

// supertest needs application to perform integration test
const app = require('../app'); 

const Book = mongoose.model('Book');

process.env.ENV = 'Test';


// supertest agent
const agent = request.agent(app);


describe('Book CRUD test', () => {
    it('should allow a Book to be POST and return read and _id', (done) => {
        const bookPost = {
            title: 'Test Book',
            author: 'Test Author',
            genre: 'Fiction'
        };

        // black box HTTP test to api
        agent.post('/api/books')
            .send(bookPost)
            .expect(200)
            .end((err, results) => {
                //console.log(results);
                //results.body.read.should.not.equals(false);
                results.body.should.have.property('_id');
                done(); // test complete
            });

    });

    // after each test, cleanup db data
    afterEach((done) => {
        Book.deleteMany({}, (err) => { 
            console.log(err); 
        }).exec();
        done();
    });

    // when at the end of test
    after((done) => {
        mongoose.connection.close(); // close db connection
        app.server.close(done()); // get server listener return and pass done() into it
    });
});