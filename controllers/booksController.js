/* eslint-disable no-underscore-dangle */

const debug = require('debug')('app:booksController');

function booksController(Book) {
    function post(req, res) {
        const book = new Book(req.body);

        if(!req.body.title){
            res.status(400);
            return res.send('Title is required');
        }
        debug(book);
        book.save();
        res.status(201);
        return res.json(book); // 201 CREATED
    }

    function get(req, res) {
        const query = {};
        // const { query } = req; // destructure 'query' out of req object stream

        if (req.query.genre) {
            query.genre = req.query.genre;
        }
        Book.find(query, (err, books) => {
            if (err) {
                return res.send(err);
            }
            const returnBooks = books.map((book) => {
                const newBook = book.toJSON(); // get pure JSON of the mongoose model
                newBook.links = {}; 
                newBook.links.self = `http://${req.headers.host}/api/books/${book._id}`; // add a link back to self
                return newBook;
            });
            return res.json(returnBooks);
        });
    }

    return {
        get,
        post,
    };
}



module.exports = booksController;