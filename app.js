const express = require('express');
const chalk = require('chalk');
const debug = require('debug')('app');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

mongoose.set('strictQuery', true);
const app = express();

console.log(`Using ENV: '${process.env.ENV}'`);


if(process.env.ENV === 'Test'){
    const db = mongoose.connect('mongodb://localhost/bookAPI_Test');
    console.log('Connected to TEST db');
} else {
    const db = mongoose.connect('mongodb://localhost/bookAPI');
    console.log('Connected to working db');
}

const port = process.env.PORT || 8000;

const Book = require('./models/bookModel');
const bookRouter = require('./routes/bookRouter')(Book); // () executing, passing in mongoose model

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api', bookRouter);

app.get('/', (req, res) => {
    res.send('Hello World API');
});

// listen returns the server listening
app.server = app.listen(port, () => {
    debug(`Server running on port ${chalk.green(port)}`);
});

module.exports = app;