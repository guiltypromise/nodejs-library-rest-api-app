/* eslint-disable no-param-reassign */
const express = require('express'); // need access to router
const debug = require('debug')('app:booksRouter');

// import BooksController reference
const booksController = require('../controllers/booksController');

function routes(Book) {
    const bookRouter = express.Router();

    // execute controller and pass Book mongoose model into it
    const controller = booksController(Book); 
    // look into bookAPI database in the Book collection
    bookRouter.route('/books')
        // create a book
        .post(controller.post)
        // read books
        .get(controller.get);

    // middleware to use findById reuse
    bookRouter.use('/books/:bookId', (req, res, next) => {
        Book.findById(req.params.bookId, (err, book) => {
            if (err) {
                return res.send(err);
            }
            if (book) {
                req.book = book;
                return next();
            }
            return res.sendStatus(404);
        });
    });

    bookRouter.route('/books/:bookId') // give us param var we can reference and use to find
        // read a single book
        .get((req, res) => {
            const returnBook = req.book.toJSON();

            // implement Hypermedia links, HATEOAS
            returnBook.links = {};
            const genre = req.book.genre.replace(' ', '%20');
            returnBook.links.FilterByThisGenre = `http://${req.headers.host}/api/books/?genre=${genre}`;
            res.json(returnBook);
        })
        // update a single, entire book
        .put((req, res) => {
            const { book } = req; // destructure book out of req.book

            book.title = req.body.title;
            book.author = req.body.author;
            book.genre = req.body.genre;
            book.read = req.body.read;
            book.save();

            req.book.save((err) => {
                if(err){
                    return res.send(err);
                }
                return res.json(book);
            });
        })
        .patch((req, res) => {
            const { book } = req;

            // eslint-disable-next-line no-underscore-dangle
            if(req.body._id){
                // eslint-disable-next-line no-underscore-dangle
                delete req.body._id;
            }

            // use Object entries to pull out only specific items
            Object.entries(req.body).forEach((item) => {
                const key = item[0];
                const value = item[1];

                book[key] = value;
            });
            req.book.save((err) => {
                if(err){
                    return res.send(err);
                }
                return res.json(book);
            });
        })
        .delete((req, res) => {
            req.book.remove((err) => {
                if(err){
                    return res.send(err);
                }
                return res.sendStatus(204); // 204 removed
            });
        });

    return bookRouter;
}

module.exports = routes;